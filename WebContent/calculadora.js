Calculadora = function() {};
 
Calculadora.prototype.somar = function(valor1, valor2) {
        return valor1 + valor2;
}
 
Calculadora.prototype.subtrair = function(valor1, valor2) {
    return valor1 - valor2;
}
 
Calculadora.prototype.multiplicar = function(valor1, valor2) {
    return valor1 * valor2;
}
 
Calculadora.prototype.dividir = function(valor1, valor2) {
    return valor1 / valor2;
}
 
Calculadora.prototype.gerarMedia = function(valor1, valor2) {
    return (valor1 + valor2) / 2;
}
 
Calculadora.prototype.fatorial = function(valor) {
    if (valor < 0) {
        throw new Error("Não é possível calcular o fatorial de um numero negativo");
    } else if (valor == 1 || valor == 0) {
        return 1;
    } else {
        return valor * this.fatorial(valor - 1);
    }
}