http://jasmine.github.io/2.0/introduction.html

//Entendendo a estrutura
describe("Teste nivel 1", function() {
    beforeEach(function() {
        console.log("beforeEach nivel 1");
    });
    describe("Teste nivel 2", function() {
        beforeEach(function() {
            console.log("beforeEach nivel 2");
        });
        describe("Teste nivel 3", function() {
            beforeEach(function() {
                console.log("beforeEach nivel 3");
            });
            it("Spec do nivel 3", function() {
                console.log("Spec do nivel 3");
                expect(true).toBe(true);
            });
            afterEach(function() {
                console.log("afterEach nivel 3");
            });
        });
        afterEach(function() {
            console.log("afterEach nivel 2");
        });
    });
    afterEach(function() {
        console.log("afterEach nivel 1");
    });
});

describe("Calculadora", function() {
    var calc;
    //Esta função vai executar antes de rodar cada Spec
    beforeEach(function() {
        calc = new Calculadora();
    });
    describe("Teste da calculadora", function(){
         
        //Spec para operação de soma
        it("Soma dos valores 3 e 5", function() {
            expect(calc.somar(3,5)).toEqual(8);
        });
 
        //Spec para operação de multiplicação
        it("Testa a multiplicação dos numeros 10 e 40", function() {
            expect(calc.multiplicar(10, 40)).toEqual(400);
        });
 
        //Spec para operação de fatorial com numero positivo
        it("Testa o fatorial de 9", function() {
            expect(calc.fatorial(9)).toEqual(362880);
        });
         
        //Spec para operação de fatorial com numero negativo
        it("Gera um throw quando o numero é negativo", function() {
            expect(function() { 
                calc.fatorial(-7)
            }).toThrowError(Error);
        });
        
		//Spec para comparação de valores utilizando === como operador
        it("Comparação de valores da função fatorial utilizando === como operador", function() {
            expect(calc.fatorial(3)).toBe(6);
        });
        
		//Spec para comparação de valores utilizando === como operador porém negando devido ao .not
        it("Comparação de valores da função fatorial utilizando === como operador porém negando devido ao .not", function() {
            expect(calc.fatorial(3)).not.toBe("6");
        });
         
    });
});

describe("Calculadora - Utilização do spyOn", function() {
    var calc;
 
    beforeEach(function() {
        calc = new Calculadora();
        spyOn(calc, 'somar');
    });
 
    describe("Espionando um objeto da calculadora", function(){
         
        //Espionando as operações realizadas com a função somar
        it("Espionando as operações realizadas com a função somar", function() {
            			 
			//Executando a função somar duas vezes com valores diferentes
            calc.somar(3,5);
			calc.somar(3,4);
			
			//verifica quantas vezes a função somar foi executada
			expect(calc.somar.calls.count()).toEqual(2);
 
            //Verifica se a função somar foi executada
            expect(calc.somar).toHaveBeenCalled();
            //Verifica se a função somar foi executada alguma vez utilizando os valores 3 e 5
			expect(calc.somar).toHaveBeenCalledWith(3,5);
        });
    });
});